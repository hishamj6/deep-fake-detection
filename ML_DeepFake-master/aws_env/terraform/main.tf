
# tells terraform we're using aws for our cloud env
provider "aws" {
	region = "us-east-1"

	# credentials are stored as environment variables
}

# set up the user data script (script that runs on initial bootup)
data "template_file" "user_data" {
  template = file("user_data.tpl")
  vars = {
		ebs_size = var.ebs_vol_size
  }
}

# connects the ec2 instance to our private cloud network
resource "aws_vpc" "deepfake_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
}

# internet gateway to connect vpc to the public internet
resource "aws_internet_gateway" "deepfake_igw" {
  vpc_id = aws_vpc.deepfake_vpc.id
}

# subnet our ec2 instance lies inside the vpc in
resource "aws_subnet" "deepfake_subnet" {
  cidr_block = cidrsubnet(aws_vpc.deepfake_vpc.cidr_block, 3, 1)
  vpc_id = aws_vpc.deepfake_vpc.id
  availability_zone = "us-east-1a"
}

# route the vpc to the internet gateway
resource "aws_route_table" "deepfake_rtb" {
  vpc_id = aws_vpc.deepfake_vpc.id
	route {
		cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.deepfake_igw.id
  }

	tags = {
    Name = var.tag_name
  }
}

# route the subnet to the route table
resource "aws_route_table_association" "deepfake_rtb_association" {
  subnet_id = aws_subnet.deepfake_subnet.id
  route_table_id = aws_route_table.deepfake_rtb.id
}

# security group dictates what can go in and out of our vpc
resource "aws_security_group" "deepfake_security_group" {
	name = "allow-all-sg"
	vpc_id = aws_vpc.deepfake_vpc.id

	# allow ssh input from anywhere
	ingress {
		cidr_blocks = ["0.0.0.0/0"]
		from_port = 22
		to_port = 22
		protocol = "tcp"
	}

	# allow outward communication to anywhere
	egress {
		cidr_blocks = ["0.0.0.0/0"]
		from_port = 0
		to_port = 0
		protocol = "-1"
	}
}

# EBS is an external file system (so data isn't stored on the EC2 instance itself)
# another plus to using external file system is if we need to upscale, data is saved
resource "aws_ebs_volume" "deepfake_ebs" {
	availability_zone = "us-east-1a"
	size = var.ebs_vol_size

	tags = {
		Name = var.tag_name
	}
}

# EC2 instance is used for actual computing/processing power
resource "aws_instance" "deepfake_ec2" {
	ami = var.ec2_ami
	instance_type = var.ec2_instance_type
	key_name = var.ec2_key
	user_data = data.template_file.user_data.rendered

	subnet_id = aws_subnet.deepfake_subnet.id
	security_groups = [aws_security_group.deepfake_security_group.id]

	tags = {
		Name = var.tag_name
	}
}

// mount the EBS to the EC2 instance
resource "aws_volume_attachment" "deepfake_ebs_ec2_attach" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.deepfake_ebs.id
  instance_id = aws_instance.deepfake_ec2.id
}

# give ec2 a public ip to connect to (persistent, stays the same even if we change
# the ec2 instance
resource "aws_eip" "deepfake_ec2_eip" {
  instance = aws_instance.deepfake_ec2.id
  vpc = true
}

