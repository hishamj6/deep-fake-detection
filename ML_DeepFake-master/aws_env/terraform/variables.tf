
/**
* EBS / STORAGE VARIABLES
*/
// storage size in GiB
variable "ebs_vol_size" { default = 30 }

/**
* EC2/PROCESSOR VARIABLES
**/
// amount of processing power to allocate
variable "ec2_instance_type" { default = "m5.large" }
// machine image (i.e. OS) currently set to DL ubuntu
variable "ec2_ami" { default = "ami-0a79b70001264b442" }
// key needed to ssh into the instance (set manually)
variable "ec2_key" { default = "deepfake_key" }

/**
* TAG INFO
**/
variable "tag_name" { default = "DeepFake Machine Learning Capstone" }
