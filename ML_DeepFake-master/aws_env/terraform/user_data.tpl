#!/bin/bash

# this file will be run on initial startup of the EC2 instance
# therefore, we need to clone the github repository and set everything up
# to be played around with. installs necessary packages, downloads python modules,
# and clones the github as mentioned. look below if youre interested.

# log output of this file execution
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

# install needed packages
echo "installing packages..."
sudo apt-get update
sudo apt-get install -y --fix-missing \
	build-essential \
	cmake \
	gfortran \
	git \
	wget \
	curl \
	graphicsmagick \
	libgraphicsmagick1-dev \
	libatlas-base-dev \
	libavcodec-dev \
	libavformat-dev \
	libgtk2.0-dev \
	libjpeg-dev \
	liblapack-dev \
	libswscale-dev \
	pkg-config \
	python3 \
	python3-pip \
	python3-dev \
	python3-numpy \
	software-properties-common \
	zip \
	xvfb \
	&& apt-get clean && rm -rf /tmp/* /var/tmp/*
echo "done installing packages."

# build specific packages
echo "building required packages..."
echo "building dlib:"
cd ~ && \
    mkdir -p dlib && \
    git clone -b 'v19.9' --single-branch https://github.com/davisking/dlib.git dlib/ && \
    cd  dlib/ && \
    python3 setup.py install --yes USE_AVX_INSTRUCTIONS
echo "done building dlib."
echo "done building packages."

# mount the ebs volume
echo "mounting the ebs volume..."
drive=`lsblk | grep ${ebs_size} | cut -f1 -d" "`
if ! sudo file -s /dev/$drive | grep -q filesystem; then
	sudo mkfs -t xfs /dev/$drive
fi
cd /home/ubuntu
sudo mkdir ebs
sudo mount /dev/$drive ebs
sudo chown ubuntu ebs --recursive
sudo chgrp ubuntu ebs --recursive
echo "done mounting the ebs volume."

# connect to the github
echo "cloning the git repository..."
cd /home/ubuntu
git clone https://mbrendanDeepfake:MachineLearningIsCool@github.com/mccloskeybr/ML_DeepFake.git
sudo chown ubuntu ML_DeepFake --recursive
sudo chgrp ubuntu ML_DeepFake --recursive
echo "done cloning the git repository."

# install python packages
echo "installing the python libraries..."
sudo python3 -m pip install scikit-build
sudo python3 -m pip install face_recognition
sudo python3 -m pip install pretrainedmodels
sudo python3 -m pip install tqdm
sudo python3 -m pip install torchvision
sudo python3 -m pip install opencv-python
echo "done installing the python libraries."

echo "done."
