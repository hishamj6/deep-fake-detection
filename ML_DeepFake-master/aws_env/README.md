
## How to access:

The public ip of the EC2 instance is: 52.21.140.135

Make sure you have the pem key in this directory. You can't access the EC2 instance without it. With it, you run the following command (assuming you're running it from within this directory):
```
ssh ubuntu@52.21.140.135 -i ./deepfake_key.pem
```

To copy files over (i.e. files processed through the classifier), you have to scp them to your local machine:
```
scp -i ./deepfake_key.pem ubuntu@52.21.140.135:<absolute_path_to_file> <where_to_save_file_locally>
```

## Keep in mind:

- The environment uses an elastic IP, which means that it should stay the same even if we have to up/downscale the system
- It's about 10 cents per hour to run, meaning it's about $72 per month (~$24 per person). I can start and stop it at any time, just let me know when you want me to do so.
- Upscaling and downscaling is easy, but downloading some of these python libraries takes an ungodly amount of memory, which means the current size is about right. Let me know if you want me to play around with other options. It'll take the system down for a while if I do so.


