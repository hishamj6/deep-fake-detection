
![ScreenShot](flowchart_1.png)

<a href="https://github.com/hkchengrex/PyTorch-ARCNN">PyTorch-ARCNN implementation (github)</a>

Original paper for compression artifacts reduction citation:
```
@inproceedings{
    author = {Ke Yu, Chao Dong, Yubin Deng, Chen Change Loy, Xiaoou Tang},
    title = {Deep Convolution Networks for Compression Artifacts Reduction},
    year = 2019,
    url = {http://mmlab.ie.cuhk.edu.hk/projects/ARCNN.html}
}
```

