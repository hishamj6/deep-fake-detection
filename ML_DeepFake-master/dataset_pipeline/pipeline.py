from torch.utils.data import DataLoader
from argparse import ArgumentParser
from tqdm import tqdm
from os.path import join
import numpy as np
import pandas as pd
import scipy.io
import torch
import dlib
import cv2
import os

from arcnn.model import ARCNN
from arcnn.loader import *

# arguments
parser = ArgumentParser()
parser.add_argument('--input_dir', type=str, help='Directory where videos are kept.', default="input")
parser.add_argument('--model', type=str, help='Path to artifact removal model weights', default="arcnn/weights/q40.mat")
parser.add_argument('--output_dir', type=str, help='Directory where to output high res videos.', default="output")

# torch args
device = None     # for cuda
b = 1             # batch size (for loading frames -> torch/model throughput)

'''
torch dataset for housing frames of the video
'''
class FrameDataset(Dataset):
  def __init__(self, data):
    self.data = data

  def __getitem__(self, idx):
    image = self.data[idx]
    image = bgr2ycbcr(image.astype(np.float32)/255).transpose(2, 0, 1)
    return idx, image

  def __len__(self):
    return len(self.data)

'''
load artifact removal model, initialize relevant processes (cuda)
'''
def load_model(model_path):
  global device
  weights = scipy.io.loadmat(model_path)
  device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
  net = ARCNN(weights).to(device).eval()
  return net

'''
find where the face is in a frame and return a frame to use for cropping it
'''
def get_boundingbox(face, width, height, scale=1.3, minsize=None):
    x1 = face.left()
    y1 = face.top()
    x2 = face.right()
    y2 = face.bottom()
    size_bb = int(max(x2 - x1, y2 - y1) * scale)

    if minsize:
        if size_bb < minsize:
            size_bb = minsize

    center_x, center_y = (x1 + x2) // 2, (y1 + y2) // 2

    # Check for out of bounds, x-y top left corner
    x1 = max(int(center_x - size_bb // 2), 0)
    y1 = max(int(center_y - size_bb // 2), 0)

    # Check for too big bb size for given x, y
    size_bb = min(width - x1, size_bb)
    size_bb = min(height - y1, size_bb)

    return x1, y1, size_bb

'''
runs the video through the de-blocking/artifact algorithm and saves all resulting images
'''
def preprocess_video(input_path, net, output_dir):
  # pass video path into cv2 video reader
  reader = cv2.VideoCapture(input_path)
  video_name = input_path.split('/')[-1].split('.')[0]

  face_detector = dlib.get_frontal_face_detector()

  # load in all the frames
  frames = list()
  while reader.isOpened():
    _, frame = reader.read()
    if frame is None:
      break
    height, width = frame.shape[:2]

    # get face from frame
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_detector(gray, 1)
    if (len(faces) == 0):
      continue
    face = faces[0]
    x, y, size = get_boundingbox(face, width, height)
    cropped_face = frame[y:y+size, x:x+size]

    # append to save
    frames.append(cropped_face)

  reader.release()

  # pass to torch
  dataset = FrameDataset(frames)
  loader = DataLoader(dataset, batch_size = b)

  with torch.no_grad():
    for idx, image in loader:
      # pass photos through algorithm
      image = image.to(device)
      result = net(image[:, 0:1, :, :])
      comb_result = torch.cat((result, image[:, 1:3, :, :]), 1)

      # convert tensor to img and save
      for i in range(result.shape[0]):
        output_path = os.path.abspath(os.path.join(output_dir, video_name + "_" + str(idx[i].item()) + ".png"))
        cv2.imwrite(output_path,
          (ycbcr2bgr(
            comb_result[i].detach().cpu().numpy().transpose(1, 2, 0)
          )*255+0.5).astype(np.int32))

'''
main entry point for the program
'''
def main():
  # get arguments
  args = parser.parse_args()
  input_dir = args.input_dir
  model_path = args.model
  output_dir = args.output_dir

  # load model and video paths
  net = load_model(model_path)
  video_paths = [os.path.abspath(os.path.join(input_dir, x)) for x in os.listdir(input_dir)]

  if video_paths is None or len(video_paths) == 0:
    print("ERR: No input videos detected")
    quit()

  # process each detected video
  pbar = tqdm(total = len(video_paths))
  for video_path in video_paths:
    preprocess_video(video_path, net, output_dir)
    pbar.update(1)

  pbar.close()

if __name__ == "__main__":
  main()

